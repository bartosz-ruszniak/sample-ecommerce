package com.ruszniak.sggwshopexample.catalog.web.rest;

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemRequest;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.CreateItemResponse;
import com.ruszniak.sggwshopexample.catalog.domain.items.dto.GetItemResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
class ItemResource {

    private final CatalogFacade catalogFacade;

    @GetMapping(value = "items")
    List<GetItemResponse> getAllItems() {
        return catalogFacade.getAllItemResponses();
    }

    @GetMapping(value = "items/{id}")
    ResponseEntity<GetItemResponse> getItem(@PathVariable Long id) {
        return ResponseEntity.ok(catalogFacade.getItemResponseById(id));
    }

    @PostMapping(value = "items")
    ResponseEntity<CreateItemResponse> createItem(@RequestBody CreateItemRequest request) {
        CreateItemResponse response = catalogFacade.createItem(request);
        return ResponseEntity.ok(response);
    }
}
