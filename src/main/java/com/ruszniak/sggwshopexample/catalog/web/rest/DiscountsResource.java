package com.ruszniak.sggwshopexample.catalog.web.rest;

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateDiscountResponse;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFactorDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFlatDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.GetDiscountResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
class DiscountsResource {

    private final CatalogFacade catalogFacade;

    @GetMapping("discounts")
    List<GetDiscountResponse> getAllDiscounts() {
        return catalogFacade.getAllDiscountResponses();
    }

    @GetMapping("discounts/{id}")
    ResponseEntity<GetDiscountResponse> getDiscount(@PathVariable Long id) {
        return ResponseEntity.ok(catalogFacade.getDiscountResponseById(id));
    }

    @PostMapping("discounts/flat")
    ResponseEntity<CreateDiscountResponse> createFlatDiscount(@RequestBody CreateFlatDiscountRequest request) {
        CreateDiscountResponse response = catalogFacade.createDiscount(request);
        return ResponseEntity.ok(response);
    }

    @PostMapping("discounts/factor")
    ResponseEntity<CreateDiscountResponse> createFactorDiscount(@RequestBody CreateFactorDiscountRequest request) {
        CreateDiscountResponse response = catalogFacade.createDiscount(request);
        return ResponseEntity.ok(response);
    }
}
