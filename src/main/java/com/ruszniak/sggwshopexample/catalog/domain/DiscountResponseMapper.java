package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateDiscountResponse;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.GetDiscountResponse;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountEntity;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import lombok.AllArgsConstructor;

import java.util.stream.Collectors;

@AllArgsConstructor
class DiscountResponseMapper {

    CreateDiscountResponse mapToResponseCreate(DiscountEntity discountEntity) {
        return CreateDiscountResponse
                .builder()
                .amount(discountEntity.getAmount())
                .type(discountEntity.getDiscountStrategy().getDiscountType())
                .items(discountEntity.getItemEntities().stream()
                        .map(ItemEntity::getId)
                        .collect(Collectors.toSet()))
                .build();
    }

    GetDiscountResponse mapToResponseGet(DiscountEntity discountEntity) {
        return GetDiscountResponse
                .builder()
                .amount(discountEntity.getAmount())
                .type(discountEntity.getDiscountStrategy().getDiscountType())
                .items(discountEntity.getItemEntities().stream()
                        .map(ItemEntity::getId)
                        .collect(Collectors.toSet()))
                .build();
    }
}
