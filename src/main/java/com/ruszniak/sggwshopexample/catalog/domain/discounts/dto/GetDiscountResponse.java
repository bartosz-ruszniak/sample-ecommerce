package com.ruszniak.sggwshopexample.catalog.domain.discounts.dto;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountType;
import lombok.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class GetDiscountResponse {

    private BigDecimal amount;

    private DiscountType type;

    @Builder.Default
    private Set<Long> items = new HashSet<>();
}
