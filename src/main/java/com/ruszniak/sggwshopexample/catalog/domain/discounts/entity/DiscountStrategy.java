package com.ruszniak.sggwshopexample.catalog.domain.discounts.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@EqualsAndHashCode
abstract public class DiscountStrategy {

    protected DiscountType discountType;

    public abstract BigDecimal apply(BigDecimal itemPrice, BigDecimal discountAmount, BigDecimal totalOrderPrice);
}
