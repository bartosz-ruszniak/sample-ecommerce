package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFactorDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFlatDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountEntity;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountStrategyFactor;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountStrategyFlat;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import lombok.AllArgsConstructor;

import java.util.HashSet;
import java.util.List;

@AllArgsConstructor
class DiscountRequestMapper {

    private final ItemRepository itemRepository;

    DiscountEntity mapToEntity(CreateFlatDiscountRequest createFlatDiscountRequest) {
        List<ItemEntity> itemEntities = itemRepository.findAllById(createFlatDiscountRequest.getItems());
        return DiscountEntity
                .builder()
                .amount(createFlatDiscountRequest.getAmount())
                .discountStrategy(new DiscountStrategyFlat())
                .itemEntities(new HashSet<>(itemEntities))
                .build();
    }

    DiscountEntity mapToEntity(CreateFactorDiscountRequest createFactorDiscountRequest) {
        List<ItemEntity> itemEntities = itemRepository.findAllById(createFactorDiscountRequest.getItems());
        return DiscountEntity
                .builder()
                .amount(createFactorDiscountRequest.getAmount())
                .discountStrategy(new DiscountStrategyFactor())
                .itemEntities(new HashSet<>(itemEntities))
                .build();
    }
}
