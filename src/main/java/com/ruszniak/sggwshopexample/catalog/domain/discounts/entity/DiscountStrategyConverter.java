package com.ruszniak.sggwshopexample.catalog.domain.discounts.entity;

import javax.persistence.AttributeConverter;

class DiscountStrategyConverter implements AttributeConverter<DiscountStrategy, String> {

    @Override
    public String convertToDatabaseColumn(DiscountStrategy discountStrategy) {
        return discountStrategy.getDiscountType().name();
    }

    @Override
    public DiscountStrategy convertToEntityAttribute(String discountType) {
        switch (discountType) {
            case "FLAT":
                return new DiscountStrategyFlat();
            case "FACTOR":
                return new DiscountStrategyFactor();
            default:
                return null;
        }
    }
}
