package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateDiscountResponse;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFactorDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.CreateFlatDiscountRequest;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.dto.GetDiscountResponse;
import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountEntity;
import com.ruszniak.sggwshopexample.core.exception.ObjectNotFoundException;
import com.ruszniak.sggwshopexample.core.validation.ValidationProcessor;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
class DiscountService {

    private final DiscountRepository discountRepository;
    private final DiscountRequestMapper requestMapper;
    private final DiscountResponseMapper responseMapper;
    private final ValidationProcessor validationProcessor;

    CreateDiscountResponse persistRequestCreate(CreateFlatDiscountRequest request) {
        validationProcessor.processRequest(request);

        DiscountEntity discountEntity = requestMapper.mapToEntity(request);
        discountEntity = discountRepository.save(discountEntity);
        return responseMapper.mapToResponseCreate(discountEntity);
    }

    CreateDiscountResponse persistRequestCreate(CreateFactorDiscountRequest request) {
        validationProcessor.processRequest(request);

        DiscountEntity discountEntity = requestMapper.mapToEntity(request);
        discountEntity = discountRepository.save(discountEntity);
        return responseMapper.mapToResponseCreate(discountEntity);
    }

    List<GetDiscountResponse> getAllResponses() {
        return discountRepository
                .findAll()
                .stream()
                .map(responseMapper::mapToResponseGet)
                .collect(Collectors.toList());
    }

    GetDiscountResponse getResponseById(Long id) {
        DiscountEntity discountEntity = discountRepository
                .findById(id)
                .orElseThrow(ObjectNotFoundException::new);

        return responseMapper.mapToResponseGet(discountEntity);
    }
}
