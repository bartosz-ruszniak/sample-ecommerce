package com.ruszniak.sggwshopexample.catalog.domain;

import com.ruszniak.sggwshopexample.catalog.domain.discounts.entity.DiscountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

interface DiscountRepository extends JpaRepository<DiscountEntity, Long> {
}
