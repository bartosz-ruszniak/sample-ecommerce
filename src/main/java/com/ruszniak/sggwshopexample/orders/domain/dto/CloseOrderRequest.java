package com.ruszniak.sggwshopexample.orders.domain.dto;

import lombok.*;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class CloseOrderRequest {
    private long id;
}
