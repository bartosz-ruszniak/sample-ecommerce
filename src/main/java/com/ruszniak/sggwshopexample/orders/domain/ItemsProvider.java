package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.catalog.domain.CatalogFacade;
import com.ruszniak.sggwshopexample.catalog.domain.items.entity.ItemEntity;
import com.ruszniak.sggwshopexample.orders.domain.dto.OrderedItemRequest;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

@AllArgsConstructor
class ItemsProvider {

    private final CatalogFacade catalogFacade;

    void doWithItems(Set<OrderedItemRequest> orderedItemRequests, BiConsumer<ItemEntity, OrderedItemRequest> consumer) {
        List<Long> itemIds = orderedItemRequests
                .stream()
                .map(OrderedItemRequest::getId)
                .collect(Collectors.toList());

        Map<Long, ItemEntity> itemEntityMap = catalogFacade
                .getItemEntitiesById(itemIds)
                .stream()
                .collect(Collectors.toMap(ItemEntity::getId, Function.identity()));

        orderedItemRequests
                .forEach(orderedItem -> {
                    ItemEntity itemEntity = itemEntityMap.get(orderedItem.getId());
                    consumer.accept(itemEntity, orderedItem);
                });
    }
}
