package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.orders.domain.entity.OrderedItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

interface OrderedItemRepository extends JpaRepository<OrderedItemEntity, Long> {
}
