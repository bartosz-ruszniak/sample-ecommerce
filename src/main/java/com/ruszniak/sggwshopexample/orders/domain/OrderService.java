package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.core.exception.ObjectNotFoundException;
import com.ruszniak.sggwshopexample.core.validation.ValidationProcessor;
import com.ruszniak.sggwshopexample.orders.domain.dto.*;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderEntity;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderStatus;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderedItemEntity;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
class OrderService {

    private final OrderRepository orderRepository;
    private final OrderResponseMapper responseMapper;
    private final DiscountCalculationService discountCalculationService;
    private final OrderedItemsService orderedItemsService;
    private final ValidationProcessor validationProcessor;

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    CreateOrderResponse placeAnOrder(CreateOrderRequest request) {
        validationProcessor.processRequest(request);

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setAddress(request.getAddress());
        orderEntity.setOrderStatus(OrderStatus.CREATED);

        Set<OrderedItemEntity> orderedItemEntities = orderedItemsService
                .prepareOrderedItems(request.getOrderedItems(), orderEntity);
        orderEntity.setOrderedItemEntities(orderedItemEntities);

        BigDecimal totalOrderPrice = discountCalculationService.calculateDiscountedPrice(orderEntity);
        orderEntity.setPrice(totalOrderPrice);

        orderRepository.save(orderEntity);
        orderedItemsService.updateItemsQuantity(orderEntity.getOrderedItemEntities());

        return responseMapper.mapToResponseCreate(orderEntity);
    }

    Invoice invoiceOrder(InvoiceOrderRequest request) {
        OrderEntity orderEntity = orderRepository.getOne(request.getId());
        orderEntity.setOrderStatus(OrderStatus.INVOICED);
        orderRepository.save(orderEntity);
        return Invoice
                .builder()
                .price(orderEntity.getPrice())
                .build();
    }

    List<GetOrderResponse> getAll() {
        return orderRepository
                .findAll()
                .stream()
                .map(responseMapper::mapToResponseGet)
                .collect(Collectors.toList());
    }

    GetOrderResponse getById(Long id) {
        OrderEntity orderEntity = orderRepository
                .findById(id)
                .orElseThrow(ObjectNotFoundException::new);

        return responseMapper.mapToResponseGet(orderEntity);
    }

    void closeOrder(CloseOrderRequest request) {
        OrderEntity orderEntity = orderRepository.getOne(request.getId());
        orderEntity.setOrderStatus(OrderStatus.CLOSED);
        orderRepository.save(orderEntity);
    }
}
