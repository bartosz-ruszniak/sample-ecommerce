package com.ruszniak.sggwshopexample.orders.domain;

import com.ruszniak.sggwshopexample.orders.domain.dto.CreateOrderResponse;
import com.ruszniak.sggwshopexample.orders.domain.dto.GetOrderResponse;
import com.ruszniak.sggwshopexample.orders.domain.dto.OrderedItemResponse;
import com.ruszniak.sggwshopexample.orders.domain.entity.OrderEntity;
import lombok.AllArgsConstructor;

import java.util.stream.Collectors;

@AllArgsConstructor
class OrderResponseMapper {

    CreateOrderResponse mapToResponseCreate(OrderEntity orderEntity) {
        return CreateOrderResponse
                .builder()
                .address(orderEntity.getAddress())
                .price(orderEntity.getPrice())
                .orderStatus(orderEntity.getOrderStatus())
                .orderedItemResponse(orderEntity.getOrderedItemEntities().stream()
                        .map(orderedItem -> OrderedItemResponse.builder()
                                .name(orderedItem.getItemEntity().getName())
                                .description(orderedItem.getItemEntity().getDescription())
                                .price(orderedItem.getPrice())
                                .quantity(orderedItem.getQuantity())
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    GetOrderResponse mapToResponseGet(OrderEntity orderEntity) {
        return GetOrderResponse
                .builder()
                .address(orderEntity.getAddress())
                .price(orderEntity.getPrice())
                .orderStatus(orderEntity.getOrderStatus())
                .orderedItemResponse(orderEntity.getOrderedItemEntities().stream()
                        .map(orderedItem -> OrderedItemResponse.builder()
                                .name(orderedItem.getItemEntity().getName())
                                .description(orderedItem.getItemEntity().getDescription())
                                .price(orderedItem.getPrice())
                                .quantity(orderedItem.getQuantity())
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }
}
